# APIGATEWAY Authorization and Authentication

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Opciones para implementar la autorización y autenticación
*  Method Basic Token (lambda)
*  Method Basic JWT (lambda)
*  Method Cognito

### Method Basic Token
Requisitos:
*  AWS-CLI https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html
*  AWS-SAM-CLI https://docs.aws.amazon.com/serverless-application-model/*  Python >= 2.7 https://www.python.org/downloads/

Arquitectura:
![Method Basic TOKEN](img/token.png)

Configuración: Archivo Makefile puede reemplazar los siguientes valores
```sh
OWNER=seidor 
CLOUD=aws
PROJECT=auth-basic
REGION=us-east-1
TOKENMASTER=PeelugihaereiCedabei
#BUCKET requerido
BUCKET=seidor-discovery-repository
```
Despliegue: Contiene un Makefile(Linux)
*  make deploy: Ejecutar el despliegue de la solución
*  make testgood: Verificar que Apigateway-Lambda responde de manera correcta.
*  make testbad: Muestra el mensaje cuando el usuario no envía el Token.
*  make destroy: Elimina la solución desplegada.

[![asciicast](https://asciinema.org/a/YbGrP2MZkYXc40Hp5Le0Ogt0X.svg)](https://asciinema.org/a/YbGrP2MZkYXc40Hp5Le0Ogt0X)

### Method Basic JWT
Requisitos:
  - AWS-CLI https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html
  - Node.js 8 or 10 https://nodejs.org/es/download/
  - NPM https://docs.npmjs.com/cli/install
  - Claudia.js "npm install claudia -g" https://claudiajs.com/tutorials/installing.html

Arquitectura:
![Method Basic JWT](img/jwt.png)

Configuración: Archivo Makefile puede reemplazar los siguientes valores
```sh
OWNER=seidor
CLOUD=aws
PROJECT=auth-basic
REGION=us-east-1
#URLAPIGATEWAY *Se requiere reemplazar despues de realizar el despliegue de la solución
URLAPIGATEWAY=https://xxxx.execute-api.us-east-1.amazonaws.com/latest
#TOKEN Se requiere reemplazar una vez desplegada la solución make testgenerator
TOKEN=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTU4MzcyMzgzOH0.a8qXatsgFIG71rqdrxIfLdhDqnIsxvuspHsZ5DWKK1Q
```
Despliegue: Contiene un Makefile(Linux)
*  make deploy: Ejecutar el despliegue de la solución
*  make testgenerator: Generar JWT (Reemplazar variable del Makefile URLAPIGATEWAY)
*  make testgood: Verificar que Apigateway-Lambda responde de manera correcta. (Reemplazar variable del Makefile TOKEN y URLAPIGATEWAY)
*  make testbad: Muestra el mensaje cuando el usuario no envía el Token. (Reemplazar variable del Makefile TOKEN y URLAPIGATEWAY)
*  make delete: Elimina la solución desplegada.
*  make testdecode: Decode JWT

[![asciicast](https://asciinema.org/a/VaWPaWHchWG96q1FVf97N1p1i.svg)](https://asciinema.org/a/VaWPaWHchWG96q1FVf97N1p1i)

### Method Cognito
Requisitos:
*  AWS-CLI https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html
*  Node.js 8 or 10 https://nodejs.org/es/download/
*  NPM https://docs.npmjs.com/cli/install

Arquitectura:
![Method Basic JWT](img/cognito.png)

Despliegue Backend: (folder backend)
  - npm run deploy: Ejecutar el despliegue de la solución

Despliegue Client: (folder client) Despues de realizar el despliegue del backend, la salida mostrará el siguiente resultado el cual debe ser reemplazado en el archivo method-cognito/client/src/config.js
```sh
CognitoIdentityPoolId - Cognito Identity Pool Id --- us-east-1:xxxxx-xxxxx-xxxx-xxxx-xxxxxxxxxxx
CognitoUserPoolClientId - Cognito User Pool Client Id ---  xxxxxxxxxxxxxxxxxxxxxxx
ApiUrl - API endpoint URL for Prod environment --- https://xxxx.execute-api.us-east-1.amazonaws.com/dev/
CognitoUserPoolId - Cognito User Pool Id --- us-east-1_xxxxxx
Region - Region --- us-east-1
ApiId - API ID ---  xxxxxx
```
method-cognito/client/
*  npm install: Ejecutar la descarga de las librerias requeridas
*  npm start

[![asciicast](https://asciinema.org/a/xB2qzXJBkzDaxdQ69LJbzyeGb.svg)](https://asciinema.org/a/xB2qzXJBkzDaxdQ69LJbzyeGb)