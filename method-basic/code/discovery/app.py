import json, os, concurrent.futures
from chalice import Chalice, CustomAuthorizer, AuthResponse
app = Chalice(app_name='aplication')

############ FOR CLIENT #############
@app.route('/aplicacion', methods=['GET'],cors=True)
def clientregion():
    tipo=app.current_request.query_params.get('tipo')
    return json.dumps({'data': str(tipo)} ,sort_keys=True, default=str)
