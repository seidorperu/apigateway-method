import os
varToken  = os.environ['TOKEN']
def generate_policy(principal_id, effect=None, resource=None):
    auth_response = {
        'principalId': principal_id
    }

    if effect and resource:
        auth_response['policyDocument'] = {
            'Version': '2012-10-17',
            'Statement': [
                {
                    'Action': 'execute-api:Invoke',
                    'Effect': effect,
                    'Resource': resource
                }
            ]
        }

    return auth_response


def lambda_handler(event, context):
    token = event['authorizationToken']
    method_arn = event['methodArn']

    if token == 'Bearer '+varToken:
        return generate_policy(token, 'Allow', '*')
    else:
        raise Exception('Unauthorized STOP')
