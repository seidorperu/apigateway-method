export default {
	apiGateway: {
		REGION: 'XXXXX',
		URL: 'https://xxxxxx.execute-api.us-east-1.amazonaws.com/XXX'
	},
	cognito: {
		REGION: 'XXXX',
		USER_POOL_ID: 'XXXX_XXXXX',
		APP_CLIENT_ID: 'XXXXXXXXXXXXXXXXXXXX',
		IDENTITY_POOL_ID: 'XXXX:XX-XX-XXX-XXX-XXXXXX'
	}
};
