var jwt = require('jsonwebtoken');

exports.auth = function auth(event, context, callback) {
    'use strict';
    console.log('got event', event);

    try {
        var decoded = jwt.verify(event.authorizationToken, 'keyboard cat');

    } catch (e) {

        callback('Unauthorized');
        return;
    }

    const userId = decoded.userId;
    let methodArn = event.methodArn;

    let tmp = methodArn.split(':'),
        apiGatewayArnTmp = tmp[5].split('/'),
        awsAccountId = tmp[4],
        region = tmp[3],
        restApiId = apiGatewayArnTmp[0],
        stage = apiGatewayArnTmp[1];

    //// create IAM Policy
    const policy = {
        'principalId': userId,
        'policyDocument': {
            'Version': '2012-10-17',
            'Statement': [{
                'Effect': 'Allow',
                'Action': [
                    'execute-api:Invoke'
                ],
                'Resource': [
                    'arn:aws:execute-api:' + region + ':' + awsAccountId + ':' + restApiId + '/' + stage + '/GET/self'
                ]
            }]
        }
    };



    if (event && event.authorizationToken && event.methodArn) {
        //// return IAM Policy
        callback(null, policy);
    } else {
        /// request is not authorized
        callback('Unauthorized');
    }
};