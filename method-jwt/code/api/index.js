var ApiBuilder = require('claudia-api-builder'),
    api = new ApiBuilder(),
    jwt = require('jsonwebtoken');


module.exports = api;

api.registerAuthorizer('jwtAuth', {
    lambdaName: 'aws-seidor-apigateway2-auth',
    lambdaVersion: true
});

api.post('/login', function(request) {
    return new Promise((resolve, reject) => {

        let userId = 1;
        let token = jwt.sign({
            userId
        }, 'keyboard cat'); /// signing the userId

        resolve({
            token
        });
    });

});


api.get('/self', function(request) {
    return new Promise((resolve, reject) => {
        var userId = request.context.authorizerPrincipalId; ///userId

        if (userId == 1) {
            var user = {
                name: "Seidor",
                rol: "sysAdm",
                userId: userId
            };

            resolve(user); /// return user
            return;
        }

        reject('User does not exist');

    });
}, {
    customAuthorizer: 'jwtAuth'
});